#!/bin/bash 

#script banner#
echo -e "\033[5;33mThis script will install java on your local machine\033[0;33m" | awk '{print $0, "\n"}'

#downloads and installs update
#then installs the default java package
apt update > /dev/null
apt install -y default-jre > /dev/null

java_version=$(java --version | grep "open" | awk '{print substr($2,1)}' | awk 'BEGIN {FS="."};{print($1)}')

java_full_version=$(java --version | grep "open" | awk '{print substr($2,1)}') 

#conditional_statements

if [ "$java_version" == "" ] #checks if the output of variable java_version is blank. i.e installation failed at some point. 
then
    echo "installing java has failed. No java version found" 
elif [ "$java_version" -lt 11 ]
then 
    echo "An outdated version of java was installed, Version: $java_full_version" 
elif [ "$java_version" -ge 11 ]  #checks if version is greater or equal to 11
then 
    echo "The version of Java installed is $java_full_version"
fi 